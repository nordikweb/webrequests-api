var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var concatCss = require('gulp-concat-css');
var uglifyCss = require('gulp-uglifycss');
var stripCssComments = require('gulp-strip-css-comments');
var sourcemaps = require('gulp-sourcemaps');
var uglifyJs = require('gulp-uglify');
var pump = require('pump');
var useref = require('gulp-useref');
var cachebust = require('gulp-cache-bust');
var clean = require('gulp-clean');
var sequence = require('run-sequence');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var zip = require('gulp-zip');

var timestamp = String(+ new Date());

// ---------------------------------------------------------
//
//
//  ------------ Development Tasks
//
//
// ---------------------------------------------------------
// Compile Sass
gulp.task('devSass', function(){
  return gulp.src('resources/assets/sass/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/css'));
});

// ---------------------------------------------------------
//
//
//  ------------ Build Tasks
//
//
// ---------------------------------------------------------

gulp.task('build', function(callback){
  sequence('copySrc', 'removeOldServerConfigs', 'copyServerConfigs', 'finalClean', 'buildZip', 'removeBuildFolder');
});

// Copy src folder to a build folder
gulp.task('copySrc', function(){
  return gulp.src(['**/.*', '**/*', '.*/*'])
    .pipe(gulp.dest('build'));
});

// Combine CSS
gulp.task('concatCss', function(){
  return gulp.src(['public/css/normalize.min.css', 'public/css/animate.css', 'public/css/lightbox.css', 'public/css/custom.css'])
    .pipe(concatCss('build.css'))
    .pipe(gulp.dest('build/public/css'));
});

// Minify CSS
gulp.task('minifyCss', function(){
  return gulp.src('build/public/css/build.css')
    .pipe(uglifyCss())
    .pipe(rename('build-' + timestamp + '.css'))
    .pipe(gulp.dest('build/public/css/'));
});

// Combine Javascript
gulp.task('combineJs', function(){
  return gulp.src([
    'public/js/vendor/jquery.waypoints.min.js',
    'public/js/vendor/sticky-sidebar.js',
    'public/js/vendor/classie.js',
    'public/js/vendor/easyResponsiveTabs.js',
    'public/js/vendor/jquery.bxslider.min.js',
    'public/js/vendor/jquery.masonry.min.js',
    'public/js/vendor/jquery.matchHeight.min.js',
    'public/js/vendor/jquery.mask.min.js',
    'public/js/vendor/jquery.collapse.js',
    'public/js/vendor/slick.min.js',
    'public/js/vendor/lightbox.js',
    'public/js/vendor/validate.min.js',
    'public/js/app/dictionary.js',
    'public/js/app/global.js',
    'public/js/app/greenon_forms.js',
    'public/js/app/forms.js',
    'public/js/app/google-maps.js',
  ])
  .pipe(sourcemaps.init())
  .pipe(concat('build.js'))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest('build/public/js/'));
});

// Minify Javascript
gulp.task('minifyJs', function(callback){

  pump([
    gulp.src('build/public/js/build.js'),
    uglifyJs(),
    rename('build-' + timestamp + '.js'),
    gulp.dest('build/public/js/')
  ], callback);

});

gulp.task('bustJsCache', function () {
    var filename = 'build-' + timestamp + '.js';

    return gulp.src('build/resources/views/layout/partials/footer_js.blade.php', { base: './' }) //must define base so I can overwrite the src file below.
        .pipe(replace(/<!--build_js-->([^<]*(?:<(?!!--end_build_js-->)[^<]*)*)<!--end_build_js-->/g, '<script id="build-js" src="/js/' + filename + '"></script>'))
        .pipe(gulp.dest('./')); //Write the file back to the same spot.
});

gulp.task('bustCssCache', function () {
    var filename = 'build-' + timestamp + '.css';

    return gulp.src('build/resources/views/layout/base.blade.php', { base: './' }) //must define base so I can overwrite the src file below.
        .pipe(replace(/<!--build_css-->([^<]*(?:<(?!!--end_build_css-->)[^<]*)*)<!--end_build_css-->/g, '<link id="build-css" href="/css/' + filename + '" rel="stylesheet">'))  //so find the script tag with an id of bundle, and replace its src.
        .pipe(gulp.dest('./')); //Write the file back to the same spot.
});

// Remove unused Javascript
gulp.task('removeUnusedJs', function(){
  return gulp.src([
    'build/public/js/vendor/jquery.waypoints.min.js',
    'build/public/js/vendor/sticky-sidebar.js',
    'build/public/js/vendor/classie.js',
    'build/public/js/vendor/easyResponsiveTabs.js',
    'build/public/js/vendor/jquery.bxslider.min.js',
    'build/public/js/vendor/jquery.masonry.min.js',
    'build/public/js/vendor/jquery.matchHeight.min.js',
    'build/public/js/vendor/jquery.mask.min.js',
    'build/public/js/vendor/jquery.collapse.js',
    'build/public/js/vendor/slick.min.js',
    'build/public/js/vendor/lightbox.js',
    'build/public/js/vendor/validate.min.js',
    'build/public/js/app/dictionary.js',
    'build/public/js/app/global.js',
    'build/public/js/app/greenon_forms.js',
    'build/public/js/app/forms.js',
    'build/public/js/app/google-maps.js',
  ], {read : false})
  .pipe(clean());
});

// Remove unused CSS
gulp.task('removeUnusedCss', function(){
  return gulp.src('build/public/css', {read : false})
    .pipe(clean());
});

gulp.task('removeOldServerConfigs', function(){
  return gulp.src('build/.env').pipe(clean());
});

// Copy over info.php and .htaccess files.
gulp.task('copyServerConfigs', function(){
  return gulp.src('env_files/.env.prod').pipe(rename('.env')).pipe(gulp.dest('build/'));
});

// Final Clean of files
gulp.task('finalClean', function(){
  return gulp.src('build/node_modules').pipe(clean());
});

// Zip up build folder with today's date using the correct format.
gulp.task('buildZip', function(){
  return gulp.src(['build/**/.*', 'build/**/*', 'build/*/.*/*'], { nodir : true})
    .pipe(zip('build-' + timestamp + '-webrequests.zip'))
    .pipe(gulp.dest(''));
});

// Remove Build folder
gulp.task('removeBuildFolder', function(){
  return gulp.src('build').pipe(clean());
});
