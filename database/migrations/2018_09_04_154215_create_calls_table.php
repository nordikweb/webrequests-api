<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->unsigned()->nullable();
            $table->string('name')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);
            $table->text('message')->nullable()->default(null);
            $table->string('form_id')->nullable()->default(null);
            $table->datetime('timestamp')->nullable()->default(null);
            $table->string('ip_address')->nullable()->default(null);
            $table->string('browser')->nullable()->default(null);
            $table->boolean('is_mobile')->nullable()->default(null);
            $table->text('conversion_path')->nullable()->default(null);
            $table->text('referrer')->nullable()->default(null);
            $table->integer('call_type_id')->unsigned()->nullable();
            $table->integer('origin_call_id')->unsigned()->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
