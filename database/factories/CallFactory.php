<?php

use Faker\Generator as Faker;

$factory->define(App\Call::class, function (Faker $faker) {
  return [
      'website_id' => $faker->numberBetween(1, 10),
      'name' => $faker->name,
      'phone' => $faker->e164PhoneNumber(),
      'email' => $faker->unique()->safeEmail,
      'form_id' => 'home-banner',
      'timestamp' => $faker->iso8601(),
      'ip_address' => $faker->ipv4(),
      'browser' => $faker->userAgent(),
      'is_mobile' => $faker->boolean(),
      'conversion_path' => '*',
      'referrer' => 'google.com',
      'call_type_id' => 1,
      'origin_call_id' => null
  ];
});
