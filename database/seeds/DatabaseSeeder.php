<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // ------------------
        // SEED WEBSITES
        // -----------------

        DB::table('websites')->insert([
          'name_en' => 'Verdun Windows & Doors',
          'name_fr' => 'Portes et Fenêtres Verdun',
          'uid' => 'verdun',
          'url' => 'https://verdunwindows.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+verdun@nordik.com',
          'automailer_email' => 'automailer@verdunwindows.com',
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Portes et Fenêtres Guitard',
          'name_fr' => 'Portes et Fenêtres Guitard',
          'uid' => 'guitard',
          'url' => 'https://www.portesetfenetresguitard.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+guitard@nordik.com',
          'automailer_email' => 'automailer@portesetfenetresguitard.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Ottawa Windows & Doors',
          'name_fr' => 'Ottawa Windows & Doors',
          'uid' => 'owd',
          'url' => 'https://ottawawindows.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+owd@nordik.com',
          'automailer_email' => 'automailer@ottawawindows.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Lambden Windows & Doors',
          'name_fr' => 'Lambden Windows & Doors',
          'uid' => 'lambden',
          'url' => 'https://www.lambden.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+lambden@nordik.com',
          'automailer_email' => 'automailer@lambden.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Northern Comfort Windows & Doors',
          'name_fr' => 'Northern Comfort Windows & Doors',
          'uid' => 'nc',
          'url' => 'https://www.northerncomfortwindows.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+nc@nordik.com',
          'automailer_email' => 'automailer@northerncomfortwindows.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Beverley Hills Windows & Doors',
          'name_fr' => 'Beverley Hills Windows & Doors',
          'uid' => 'bh',
          'url' => 'https://www.beverleyhillshome.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+bh@nordik.com',
          'automailer_email' => 'automailer@beverleyhillshome.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => "Consumer's Choice Windows & Doors",
          'name_fr' => "Consumer's Choice Windows & Doors",
          'uid' => 'cc',
          'url' => 'https://www.consumerschoice.ca',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+cc@nordik.com',
          'automailer_email' => 'automailer@consumerschoice.ca'
        ]);

        DB::table('websites')->insert([
          'name_en' => "Nordik Windows & Doors",
          'name_fr' => "Nordik Windows & Doors",
          'uid' => 'nordik',
          'url' => 'https://nordik.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+nordik@nordik.com',
          'automailer_email' => 'automailer@nordik.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Northern Comfort Windows & Doors - Newmarket',
          'name_fr' => 'Northern Comfort Windows & Doors - Newmarket',
          'uid' => 'nc-nm',
          'url' => 'https://www.northerncomfortwindows.com',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry+nc@nordik.com',
          'automailer_email' => 'automailer@northerncomfortwindows.com'
        ]);

        DB::table('websites')->insert([
          'name_en' => 'Unknown',
          'name_fr' => 'Unknown',
          'uid' => 'development',
          'url' => 'http://localhost',
          'api_key' => Hash::make(uniqid()),
          'system_admin_email' => 'bmistry@nordik.com',
          'automailer_email' => 'test@nordik.com',
        ]);


        // ------------------
        // SEED CALL TYPES
        // -----------------

        DB::table('call_types')->insert([
          'name' => "quote_request"
        ]);

        DB::table('call_types')->insert([
          'name' => "contact_form",
        ]);

        DB::table('call_types')->insert([
          'name' => "ask_an_expert",
        ]);

    }
}
