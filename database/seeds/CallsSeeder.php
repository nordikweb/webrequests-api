<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

use App\Call;


class CallsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      $faker = Faker::create();

      for ($i=0; $i < 750; $i++) {
        Call::create([
          'website_id' => $faker->numberBetween(1,10),
          'name' => $faker->name,
          'phone' => $faker->e164PhoneNumber(),
          'email' => $faker->unique()->safeEmail,
          'form_id' => 'home-banner',
          'timestamp' => Carbon::now()->subdays(rand(1, 365))->toDateTimeString(),
          'ip_address' => $faker->ipv4(),
          'browser' => $faker->userAgent(),
          'is_mobile' => $faker->boolean(),
          'conversion_path' => '*',
          'referrer' => 'google.com',
          'call_type_id' => 1,
          'origin_call_id' => null
        ]);
      }
    }
}
