<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>Merci pour votre demande!</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
	<!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->
        <!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> -->
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

	<!-- CSS Reset -->
    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What is does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */
        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }

        /* What it does: A work-around for Gmail meddling in triggered links. */
        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }

        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
	        display: none !important;
	        opacity: 0.01 !important;
        }
        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
	        display:none !important;
	   	}

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }

    </style>

    <!-- Progressive Enhancements -->
    <style>

        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

		a {
			color: #0078ae;
		}

    </style>

</head>
<body width="100%" bgcolor="#ffffff" style="margin: 0; mso-line-height-rule: exactly;">
    <center style="width: 100%; background: #ffffff; text-align: left;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: 'Open Sans', sans-serif;">
            Merci pour votre demande! Votre première étape pour obtenir des fenêtres et des portes de remplacement de haute qualité est terminée!
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!--
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
        <div style="max-width: 600px; margin: auto;" class="email-container">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]-->

            <!-- Email Header : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">
                <tr>
                    <td style="padding: 20px 0; text-align: center">
                        <img src="https://www.portesetfenetresguitard.com/img/logo-black.png" width="200" height="50" alt="Portes et Fen&ecirc;tres Guitard" border="0" style="height: auto;">
                    </td>
                </tr>
            </table>
            <!-- Email Header : END -->

            <!-- Email Body : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 600px;">

                <!-- Hero Image, Flush : BEGIN -->
                <tr>
                    <td bgcolor="#ececec" align="center" style="padding: 20px;">
                        <img src="http://vg-signatures.s3-website-us-east-1.amazonaws.com/guitard/guillaume.png" width="60" height="" alt="Guitard Guillaume" border="0" align="center" style="width: 100%; max-width: 60px; height: auto;" class="g-img"> <h1 style="display:inline-block; color:#036; font-family:'Open Sans', sans-serif; font-weight:700; font-size:34px; padding-left:10px; margin: 5px 0 0 0; vertical-align: middle; line-height: 1.1;">Votre demande a été reçue!</h1>
                    </td>
                </tr>
                <!-- Hero Image, Flush : END -->

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#f4f4f4">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td style="padding: 40px; font-family: 'Open Sans', sans-serif; font-size: 15px; line-height: 22px; color: #555555;">
                                    <span style="font-size: 17px;">Merci pour votre demande <b>{{ $call->name }}</b>!</span>
									<br><br>
									Votre première étape pour obtenir des fenêtres et des portes de remplacement de haute qualité est terminée! <i>Et maintenant?</i>&nbsp;&nbsp;Un de nos représentants vous contactera dans la prochaine journée ouvrables afin de mieux comprendre votre projet et d'établir votre soumission gratuit. C'est si simple.
                                    <br><br>
									En attendant, si vous avez des questions concernant les installations, notre processus de fabrication, ou si vous voulez en savoir plus sur Guitard, <a href="mailto:guitard@verdunwindows.com">vous pouvez nous envoyer un courriel</a> ou appelez-nous au <a href="tel:819-777-6622">(819) 777-6622</a>.
									<br><br>
									Merci pour votre intérêt dans Guitard Portes et Fenêtres!
                                </td>
                                </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : BEGIN -->

				<!-- Clear Spacer : BEGIN -->
                <tr>
                    <td height="40" style="font-size: 0; line-height: 0;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->

                <!-- 2 Even Columns : BEGIN -->
                <tr>
                    <td bgcolor="#f4f4f4" align="center" height="100%" valign="top" width="100%" style="padding-bottom: 40px; padding-top: 40px;">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:560px;">
                            <tr>
                                <td align="center" valign="top" width="50%">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                        <tr>
                                            <td style="text-align: center; padding: 0 10px;">
                                                <a href="https://www.portesetfenetresguitard.com/fenetres/"><img src="https://www.portesetfenetresguitard.com/img/featured/window.png" height="160" width="" alt="Fenêtres Guitard" border="0" align="center"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 21px; color: #555555; padding: 10px 10px 0;" class="stack-column-center">
                                                Nous facilitons l'achat de nouvelles fenêtres. Jetez un œil à la variété de styles que nous offrons.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" valign="top" width="50%">
                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                        <tr>
                                            <td style="text-align: center; padding: 0 10px;">
                                                <a href="https://www.portesetfenetresguitard.com/portes/"><img src="https://www.portesetfenetresguitard.com/img/doors/steel-entry.png" height="160" width="" alt="Portes Guitard" border="0" align="center"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center;font-family: 'Open Sans', sans-serif; font-size: 14px; line-height: 21px; color: #555555; padding: 10px 10px 0;" class="stack-column-center">
                                                Que vous souhaitiez rénover vos portes actuelles ou simplement en acheter de nouvelles, Guitard est là pour vous.
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- Two Even Columns : END -->

            </table>
            <!-- Email Body : END -->

            <!-- Email Footer : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">
                <tr>
                    <td style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: 'Open Sans', sans-serif; line-height:18px; text-align: center; color: #333333;" class="x-gmail-data-detectors">
                        Gestion Guitard<br>1706 Chemin Pink Gatineau, QC J9J 3N7<br>T: (819) 777-6622<br>TF: +1 800-267-3510<br><a href="https://www.portesetfenetresguitard.com/">portesetfenetresguitard.com</a><br>
                    </td>
                </tr>
            </table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>
