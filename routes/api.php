<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Webrequsts API v1
|
| Authentication:
| Bearer token should be passed in through the header using the
| 'Authentication' Key. Tokens are generated using the DatabaseSeeder
| class when the application is launched.
|--------------------------------------------------------------------------
|
*/
Route::prefix('v1')->group(function(){

  // Form Submissions
  Route::post('saveQuoteRequestSubmission', 'FormSubmissionController@quoteRequest');
  Route::post('saveContactFormSubmission', 'FormSubmissionController@contactForm');
  Route::post('saveAskAnExpertFormSubmission', 'FormSubmissionController@askAnExpert');

  // Calls
  Route::get('getCalls', 'CallController@getCalls');
  Route::get('getCall/{id}', 'CallController@getCall');

  // Customers
  Route::get('getCustomers', 'CustomerController@getCustomers');
  Route::get('getCustomer/{id}', 'CustomerController@getCustomer');
  Route::get('getCustomerCalls/{id}', 'CustomerController@getCustomerCalls');

  // Web Calls
  Route::get('getWebCalls', 'CallController@getWebCalls');
});
