<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Call extends Model
{
  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
  protected $fillable = [
    // Lead Identification Information
    'name', 'email', 'phone', 'address', 'message',

    // Internal IDs
    'call_type_id', 'website_id',

    // Device Tracking Information
    'ip_address', 'browser', 'is_mobile',

    // Tracking Information
    'form_id', 'conversion_path', 'referrer',

    //Timestamp
    'timestamp', 'origin_call_id'
  ];


  /**
  * The website associated with the call
  *
  */
  public function website(){

    return $this->belongsTo('App\Website');

  }

  /**
  * The call type associated with the call
  *
  */
  public function callType(){

    return $this->belongsTo('App\CallType');

  }

  /**
  * The emails associated with the call
  *
  */
  /**
  * The calls associated with the website
  *
  */
  public function emails(){

    return $this->belongsToMany('App\Email', 'call_email', 'call_id', 'email_id');

  }


}
