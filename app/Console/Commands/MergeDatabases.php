<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

use App\Call;

class MergeDatabases extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:merge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Smart merge of databases across brands.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('-----------------');
        $this->info('Merge Database Command Activated.');
        $this->info('-----------------');

        $databases = [
          'Verdun' => 'verdun',
          'Nordik' => 'nordik',
          'Consumers Choice' => 'cc',
          'Northern Comfort' => 'nc',
          'Beverley Hills' => 'bh',
        ];

        // $databases = [
        //   'Verdun' => 'mysql_verdun',
        //   'Nordik' => 'mysql_verdun',
        //   'Consumers Choice' => 'mysql_verdun',
        //   'Northern Comfort' => 'mysql_verdun',
        //   'Beverley Hills' => 'mysql_verdun',
        // ];

        foreach ($databases as $label => $db) {

          $this->info(' --- Merging ' . $label . ' Database');

          $dbReading = DB::connection('mysql_' . $db);

          $bar = $this->output->createProgressBar($dbReading->table('calls')->count());

          $dbReading->table('calls')->orderBy('id')->chunk(100, function($calls) use ($dbReading, $bar, $db){

            foreach ($calls as $readCall) {

              $callType = 1;
              $conversion = $dbReading->table('conversions')->select('path', 'referrer')->where('call_id', $readCall->id)->get();

              if($readCall->form_id == "contact-us"){
                $callType = 2;
              }

              if(count($conversion) > 0){
                $conversionPath = $conversion[0]->path;
                $referrer = $conversion[0]->referrer;
              }else{
                $conversionPath = '';
                $referrer = '';
              }

              // Create the Array to br written
              $insertedCall = [
                'name' => $readCall->name,
                'email' => $readCall->email,
                'phone' => $readCall->phone,
                'evening_phone' => $readCall->evening_phone,
                'address' => $readCall->address,
                'message' => $readCall->message,
                'form_id' => $readCall->form_id,
                'ip_address' => $readCall->ip_address,
                'browser'=> $readCall->browser,
                'postal_code' => '',
                'conversion_path' => $conversionPath,
                'referrer' => $referrer,
                'website_id' => $readCall->website_id,
                'call_type_id' => $callType,
                'timestamp' => $readCall->timestamp,
                'origin_call_id' => $readCall->id,
              ];

              // Add the postal code if available
              if($db == 'nordik' || $db == 'cc' || $db =='bh'){
                $insertedCall['postal_code'] = $readCall ->postal_code;
              }

              // Create the call
              Call::on('mysql')->create($insertedCall);

              $bar->advance();

            }

          });

          $bar->finish();
          $this->info(' --- Merging ' . $label . ' Database Complete');

        } // END OF foreach

        $this->info(' --- Merging Complete');

    } // END OF handle()
} // END OF MergeDatabases
