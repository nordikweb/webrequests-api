<?php

namespace App\Mail;

use App\Call;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

class QuoteRequestReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The call instance.
     *
     * @var Call
     */
    public $call;

    /**
     * The call instance.
     *
     * @var websiteUid
     */
    public $websiteUid;

    /**
     * The language.
     *
     * @var lang
     */
    public $lang;

    /**
     * The automailer email address.
     *
     * @var automailerEmail
     */
    public $automailerEmail;

    /**
     * The website name.
     *
     * @var websiteName
     */
    public $websiteName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Call $call, $websiteUid, $lang, $automailerEmail, $websiteName)
    {
        $this->call = $call;
        $this->website_uid = $websiteUid;
        $this->lang = $lang;
        $this->website_automailer_email = $automailerEmail;
        $this->website_name = $websiteName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        Log::info($this->website_name);

        return $this->from($this->website_automailer_email, $this->website_name)->view('emails.' . $this->website_uid . '.' . $this->lang . '.quote_request_received');
    }
}
