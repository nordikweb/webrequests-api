<?php

namespace App\Mail;

use App\Call;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCallCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The call instance.
     *
     * @var Call
     */
    public $call;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Call $call)
    {
        $this->call = $call;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $this->subject = 'New Call Created (' . $this->call->form_id . '): ' . $this->call->name;      
      return $this->view('emails.system.new_call_created');
    }
}
