<?php

namespace App\Mail;

use App\Call;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AskAnExpertFormReceived extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The call instance.
     *
     * @var Call
     */
    public $call;

    /**
     * The website UID.
     *
     * @var websiteUid
     */
    public $websiteUid;

    /**
     * The language.
     *
     * @var lang
     */
    public $lang;

    /**
     * The automailer email address.
     *
     * @var automailerEmail
     */
    public $automailerEmail;

    /**
     * The automailer email address.
     *
     * @var websiteName
     */
    public $websiteName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Call $call, $websiteUid, $lang, $automailerEmail, $websiteName)
    {
        $this->call = $call;
        $this->website_uid = $websiteUid;
        $this->lang = $lang;
        $this->website_automailer_email = $automailerEmail;
        $this->website_name = $websiteName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => $this->website_automailer_email, 'name' => $this->website_name])->view('emails.' . $this->website_uid . '.' . $this->lang . '.ask_an_expert_form_received');
    }
}
