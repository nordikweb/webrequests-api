<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;

use App\Call;
use App\Website;

use App\Mail\QuoteRequestReceived;
use App\Mail\ContactFormReceived;
use App\Mail\AskAnExpertFormReceived;
use App\Mail\NewCallCreated;


class FormSubmissionController extends Controller
{

   /**
   * Instantiate a new controller instance.
   *
   * @return void
   */
   public function __construct()
   {
     $this->defaultValidationRules = [
       'name' => 'required',
       'email' => 'required|email',
       'form_id' => 'required',
     ];
   }

  /**
   * Process the new quote request submission.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function quoteRequest(Request $request){

    // Set the validation rules
    $validationRules = array_merge($this->defaultValidationRules, ['phone' => 'required']);

    // Validate the request
    $validator = Validator::make($request->input(), $validationRules);

    // Return an error if the validation fails
    if($validator->fails()){
      return response()->json([
        'success' => false,
        'errors' => $validator->errors(),
      ], 400);
    }

    // Set the Call Type (1 = quote_request, 2 = contact_form, 3 = ask_an_expert)
    $request->merge(['call_type_id' => 1]);

    // Save the call to the Database
    $call = $this->storeCall($request);

    // Send an email to the form submitter
    Mail::to($request->input('email'))->send(new QuoteRequestReceived($call, $request->input('website_uid'), $request->input('lang'), $request->input('website_automailer_email'), $request->input('website_name')));

    // Send a system email to the website Administrator
    Mail::to($request->input('website_system_admin_email'))->send(new NewCallCreated($call));

    // Return a response
    return response()->json(['success' => true]);

  } //  END OF quoteRequest()

  /**
   * Process the new contact form submission
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function contactForm(Request $request){

    // Set the validation rules
    $validationRules = array_merge($this->defaultValidationRules, ['phone' => 'required']);

    // Validate the request
    $validator = Validator::make($request->input(), $validationRules);

    if($validator->fails()){
      return response()->json([
        'success' => false,
        'errors' => $validator->errors()
      ], 400);
    }

    // Set the Call Type (1 = quote_request, 2 = contact_form, 3 = ask_an_expert)
    $request->merge(['call_type_id' => 2]);

    // Save the call to the Database
    $call = $this->storeCall($request);

    // Send an email to the form submitter
    // Send an email to the form submitter
    Mail::to($request->input('email'))->send(new ContactFormReceived(
      $call,
      $request->input('website_uid'),
      $request->input('lang'),
      $request->input('website_automailer_email'),
      $request->input('website_name')
    ));

    // Send a system email to the website Administrator
    Mail::to($request->input('website_system_admin_email'))->send(new NewCallCreated($call));

    // Return a response
    return response()->json(['success' => true]);

  } // END OF contactForm()


  /**
   * Process the "Ask an Expert" form submission
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function askAnExpert(Request $request){

    // Set the validation rules
    $validationRules = $this->defaultValidationRules;

    // Validate the request
    $validator = Validator::make($request->input(), $validationRules);

    if($validator->fails()){
      return response()->json([
        'success' => false,
        'errors' => $validator->errors()
      ], 400);
    }

    // Set the Call Type (1 = quote_request, 2 = contact_form, 3 = ask_an_expert)
    $request->merge(['call_type_id' => 3]);

    // Save the call to the Database
    $call = $this->storeCall($request);

    // Send an email to the form submitter
    // Send an email to the form submitter
    Mail::to($request->input('email'))->send(new AskAnExpertFormReceived($call, $request->input('website_uid'), $request->input('lang'), $request->input('website_automailer_email'), $request->input('website_name')));

    // Send a system email to the website Administrator
    Mail::to($request->input('website_system_admin_email'))->send(new NewCallCreated($call));

    // Return a response
    return response()->json(['success' => true]);

  } // END OF askAnExpert()

  /**
   * Store the call in the database.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \App\Call
   */
   private function storeCall(Request $request){

     // Create the call
     $call = Call::create([
       'name' => $request->input('name'),
       'email' => $request->input('email'),
       'phone' => $request->input('phone'),
       'evening_phone' => $request->input('evening_phone'),
       'address' => $request->input('address'),
       'message' => $request->input('message'),
       'form_id' => $request->input('form_id'),
       'ip_address' => $request->ip(),
       'browser'=> $request->server('HTTP_USER_AGENT'),
       'postal_code' => $request->input('postal_code'),
       'conversion_path' => $request->input('user_path'),
       'referrer' => $request->input('referer'),
       'website_id' => $request->input('website_id'),
       'call_type_id' => $request->input('call_type_id'),
       'timestamp' => date("Y-m-d H:i:s")
     ]);

     // Save whether it is mobile or not.
     if($request->input('isMobile') === 'true'){
       $call->is_mobile = true;
       $call->save();
     }else{
       $call->is_mobile = false;
       $call->save();
     }

     return $call;

   } // END OF storeCall()



} // END OF class
