<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Log;

class CallController extends Controller
{

  /**
  * Process the new quote request submission.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function getCalls(Request $request){

    $start = $request->query('start', 0);
    $limit = $request->query('limit', 5000);

    $calls = DB::connection('firebird')->table('CALLS')
      ->where(function($query){
        $query->where("DELETED", 0)->orWhere("DELETED", null);
      })->orderBy('CALLTIME', 'DESC')
      ->limit($limit)
      ->offset($start)
      ->get();

    return response()->json([
      'success' => true,
      'calls' => $calls
    ]);
  } // END OF getCalls()

  /**
  * Process the new quote request submission.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function getCall(Request $request, $id){

    DB::connection('firebird')->enableQueryLog();

    // Run a first query to pull the Customer
    $call = DB::connection('firebird')->table('CALLS')
      ->where(function($query){
        $query->where("CALLS.DELETED", 0)->orWhere("CALLS.DELETED", null);
      })->where('CALLS.ID', $id)
      ->select('CALLS.NAME as CALL_NAME', 'CALLS.ADDRESS', 'CALLS.CITY', 'CALLS.PROVINCE', 'CALLS.WORKPHONE', 'CALLS.HOMEPHONE', 'CALLS.MOBILEPHONE', 'CALLS.CALLTIME', 'CALLS.EMAIL', 'CALLS.CUSTOMER_ID', 'CALLS.NOTES')
      ->get();

    // If there is a valid CUSTOMER_ID go ahead query information about that customer as well.
    if(!is_null($call[0]->CUSTOMER_ID) && $call[0]->CUSTOMER_ID > 0){

      $call = DB::connection('firebird')->table('CALLS')
        ->join('CUSTOMERS', 'CALLS.CUSTOMER_ID', '=', 'CUSTOMERS.ID')
        ->where(function($query){
          $query->where("CALLS.DELETED", 0)->orWhere("CALLS.DELETED", null);
        })
        ->where('CALLS.CUSTOMER_ID', '>', 0)
        ->where('CALLS.ID', $id)
        ->select('CALLS.NAME as CALL_NAME', 'CALLS.ADDRESS', 'CALLS.CITY', 'CALLS.PROVINCE', 'CALLS.WORKPHONE', 'CALLS.HOMEPHONE', 'CALLS.MOBILEPHONE', 'CALLS.CALLTIME', 'CALLS.EMAIL', 'CUSTOMERS.ID as CUSTOMER_ID', 'CUSTOMERS.NAME as CUSTOMER_NAME', 'CALLS.NOTES')
        ->get();
    }

    Log::info(DB::connection('firebird')->getQueryLog());

    return response()->json([
      'success' => true,
      'call' => $call
    ]);
  } // END OF getCall()


}
