<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CustomerController extends Controller
{

  /**
  * Return all customers.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function getCustomers(Request $request){

    $start = $request->query('start', 0);
    $limit = $request->query('limit', 5000);

    $customers = DB::connection('firebird')->table('CUSTOMERS')
      ->where(function($query){
        $query->where("DELETED", 0)->orWhere("DELETED", null);
      })->orderBy('CREATIONDATE', 'DESC')
      ->limit($limit)
      ->offset($start)
      ->get();

    return response()->json([
      'success' => true,
      'customers' => $customers
    ]);
  } // END OF getCustomers()


  /**
  * Return information about a single customer
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function getCustomer(Request $request, $id){

    // DB::connection('firebird')->enableQueryLog();

    // Run a first query to pull the Customer
    $customer = DB::connection('firebird')->table('CUSTOMERS')
      ->where(function($query){
        $query->where("CUSTOMERS.DELETED", 0)->orWhere("CUSTOMERS.DELETED", null);
      })->where('CUSTOMERS.ID', $id)
      ->select('CUSTOMERS.NAME as CUSTOMER_NAME', 'CUSTOMERS.ADDRESS', 'CUSTOMERS.CITY', 'CUSTOMERS.PROVINCE', 'CUSTOMERS.WORKPHONE', 'CUSTOMERS.HOMEPHONE', 'CUSTOMERS.CELLPHONE', 'CUSTOMERS.CREATIONDATE', 'CUSTOMERS.EMAIL', 'CUSTOMERS.NOTES')
      ->get();

    // Log::info(DB::connection('firebird')->getQueryLog());

    return response()->json([
      'success' => true,
      'customer' => $customer
    ]);
  } // END OF getCustomer()

  /**
  * Return information about a single customer
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  Integer  $id
  * @return \Illuminate\Http\Response
  */
  public function getCustomerCalls(Request $request, $id){

    // DB::connection('firebird')->enableQueryLog();

    // Run a first query to pull the Customer
    $customerCalls = DB::connection('firebird')->table('CALLS')
      ->where(function($query){
        $query->where("CALLS.DELETED", 0)->orWhere("CALLS.DELETED", null);
      })->where('CALLS.CUSTOMER_ID', $id)
      ->select('CALLS.ID', 'CALLS.CALLTIME', 'CALLS.REFERREDBY')
      ->get();

    // Log::info(DB::connection('firebird')->getQueryLog());

    return response()->json([
      'success' => true,
      'customerCalls' => $customerCalls
    ]);
  } // END OF getCustomerCalls()

}
