<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebCallsController extends Controller
{
  /**
  * Process the new quote request submission.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function getWebCalls(Request $request){

    $start = $request->query('start', 0);
    $limit = $request->query('limit', 500);

    $calls = Call::limit($limit)->offset(30)->get();

    return response()->json([
      'success' => true,
      'calls' => $calls
    ]);
  } // END OF getWebCalls()
  
}
