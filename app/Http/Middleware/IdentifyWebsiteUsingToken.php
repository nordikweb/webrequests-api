<?php

namespace App\Http\Middleware;

use Closure;
use Log;

use App\Website;

class IdentifyWebsiteUsingToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      // Get the bearer token from the request header and verify that it exists in the DB
      $token = $request->bearerToken();

      try{

        $website = Website::where('api_key', $token)->firstOrFail();

        // Set the website name based on language
        if($request->input('lang') == 'fr'){
          $websiteName = $website->name_fr;
        }else{
          $websiteName = $website->name_en;
        }

        // Merge the website ID into the request's input array for use later on.
        $request->merge([
          'website_id' => $website->id,
          'website_uid' => $website->uid,
          'website_system_admin_email' => $website->system_admin_email,
          'website_automailer_email' => $website->automailer_email,
          'website_name' => $websiteName,
        ]);

        return $next($request);

      }catch(\Exception $e){

        // log the error
        Log::info('Website not found');
        Log::info($e);

        // Return an error response
        return response()->json([
          'success' => false,
          'errors' => 'Website not found, or API key not valid. Please check your API key and try again.'
        ], 401);

      }

    } // END OF handle()

} // END OF class

// END OF file
