<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'uid',
  ];

  /**
  * The calls associated with the website
  *
  */
  public function calls(){

    return $this->hasMany('App\Call');

  }
}
