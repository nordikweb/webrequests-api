<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'guid', 'status', 'time_sent', 'time_opened'
  ];

  /**
  * The calls associated with the email
  *
  */
  public function calls(){

    return $this->belongsToMany('App\Call', 'call_email', 'email_id', 'call_id');

  }
}
